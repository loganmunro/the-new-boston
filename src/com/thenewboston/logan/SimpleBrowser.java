package com.thenewboston.logan;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;

public class SimpleBrowser extends Activity implements OnClickListener{

	EditText url;
	WebView ourBrow;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.simplebrowser);
		
		ourBrow = (WebView) this.findViewById(R.id.wvBrowser);
		ourBrow.getSettings().setJavaScriptEnabled(true);
		ourBrow.getSettings().setLoadWithOverviewMode(true);
		ourBrow.getSettings().setUseWideViewPort(true);
		ourBrow.setWebViewClient(new ourViewClient());
		
		try {
			ourBrow.loadUrl("http://www.loganmunro.com");
		} catch (Exception e){
			e.printStackTrace();
		}
		
		Button go = (Button) this.findViewById(R.id.bGo);
		Button back = (Button) this.findViewById(R.id.bBack);
		Button refresh = (Button) this.findViewById(R.id.bRefresh);
		Button forward = (Button) this.findViewById(R.id.bForward);
		Button clearHistory = (Button) this.findViewById(R.id.bHistory);
		url = (EditText) this.findViewById(R.id.etURL);
		go.setOnClickListener(this);
		back.setOnClickListener(this);
		forward.setOnClickListener(this);
		refresh.setOnClickListener(this);
		clearHistory.setOnClickListener(this);
		
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.bGo:
			String theWebsite = url.getText().toString();
			ourBrow.loadUrl(theWebsite);
			//hiding the keyboard after using an EditText
			InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(url.getWindowToken(), 0);
			
			break;
		case R.id.bBack:
			if(ourBrow.canGoBack()) ourBrow.goBack();
			break;
		case R.id.bForward:
			if(ourBrow.canGoForward()) ourBrow.goForward();
			break;
		case R.id.bRefresh:
			ourBrow.reload();
			break;
		case R.id.bHistory:
			ourBrow.clearHistory();
			break;
		}
	}
	

	
	

}
