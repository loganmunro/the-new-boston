package com.thenewboston.logan;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

public class SharedPrefs extends Activity implements OnClickListener{

	
	EditText sharedData;
	TextView dataResults;
	public static String filename = "MySharedString";
	SharedPreferences someData;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.sharedpreferences);
		setupVariables();
		someData = this.getSharedPreferences(filename, 0);
	}

	private void setupVariables() {
		// TODO Auto-generated method stub
		this.findViewById(R.id.bSave).setOnClickListener(this);
		this.findViewById(R.id.bLoad).setOnClickListener(this);
		sharedData = (EditText) this.findViewById(R.id.etSharedPrefs);
		dataResults = (TextView) this.findViewById(R.id.tvSharedPrefs);
		
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		case R.id.bSave:
			String stringData = sharedData.getText().toString();
			SharedPreferences.Editor editor = someData.edit();
			editor.putString("sharedString", stringData);
			editor.commit();
			break;
		case R.id.bLoad:
			someData = this.getSharedPreferences(filename, 0);
			String dataReturned = someData.getString("sharedString", "Couldn't load data");
			dataResults.setText(dataReturned);
			break;
		}
		
	}

}
